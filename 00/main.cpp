
// This is a comment

/*
    This is a longer comment
    It can have multiple lines
*/


#include <iostream>
/* the above statement says we want to use
 * the 'iostream' library.  NOTE: in reality,
 * iostream is a file, and #include dumps its
 * contents here at the very beginning of compilation.
 * (this happens in the "preprocessing phase")
 * All the lines that look like #something are
 * preprocessor directives. */
/* NOTE: all this crap I'm writing will be removed
 * by the preprocessor.  (Anything between slash star
 * and star slash is a "comment") */
// In c++ you can also write comments like this (slash slash
// to the end of the line)

/* The program needs a designated starting place.  main() is it
 * most of the time. */
int main()
{
	// this will be the first line of our program to execute:
	std::cout << "Hello.\n";
	/* cout was defined in iostream.  this line will print hello 
	 * to the screen.
	 * The quotes will specify a string literal, as opposed to
	 * a variable.  By default, every word you type is a variable
	 * or keyword... */
	/* The \n is an "escape sequence" which gives a way to specify
	 * various control characters when C++ syntax would otherwise
	 * make it inconvenient.  */
	return 0;
}

/* TODO: compile this program with the command:
 *    g++ main.cpp
 * Then run it, by typing ./a.out */
/* TODO: in the "Hello.\n" string, remove the quotes, try to compile
 * again, and take note of the error messages.
 * TODO: replace the '\n' in the string with an actual newline, try
 * to compile, and see what happens. */

// vim:foldlevel=4
