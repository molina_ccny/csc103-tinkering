/*
    This is the code we wrote in class. It differs slighly from main.cpp,
    but both achieve the same results. We will go over these differences
    in the coming weeks.
*/


// Include statements request system libraries
#include <iostream>
using namespace std;

// Statement that begin with # are called "preprocessor directives"



int main() { 
    
    cout << "Hello World \n\n\n";
    
    return 0;

}





