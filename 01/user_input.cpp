#include <iostream>
using namespace std;

// Global Variables
//int x = 0, y = 0;

int main() {
    
    // Declaration & Initialization
    int x = 0;
    int y = 0;
    int z = 0;
   
    // Prompt the user for input
    cout << "Please enter two numbers: ";
    
    // Request two input values
    cin >> x >> y;
    
    // Print the result
    cout << "x = " << x << endl;
    cout << "y = " << y << endl;
    
    z = x - y; // = is the ASSIGNMENT operator
    
    //x - y = z; // WRONG!!!!!!!!!!!!!
    
    cout << "z = " << z << endl;
    
    return 0;
}