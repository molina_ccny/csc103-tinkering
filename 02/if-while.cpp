/* Flow of control:
 * Boolean expressions; if and while statements.
 * */

// first, import our usual libraries:
#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <string>
using std::string;

int main()
{
	/* if statements: execute a piece of code 0 or 1 times,
	 * conditioned upon a boolean expression. */
	cout << "How are you liking 103? ";
	string response;
	getline(cin,response);
	//if (response == "yes" || "Yes") {
	/* NOTE: the above won't work, since C++ will try to understand
	 * "Yes" as a boolean, and determine its value to be "true"
	 * (see below for details...) */
	if (response == "yes" || response == "Yes") {  // NOTE: two equals signs!
		cout << "Yay!";
		cout << "  Me too 8D\n";
	}
	else if(response == "it's ok") {
	    
        cout << "it will get better!" << endl;
        
	} else {
		cout << "T.T\n";
	}
	
	
	/* here's the general format:
	 * if (<boolean expression>) {
	 *    zero or more statements...
	 * } else if (<boolean expr>) {
	     statements...
	 * } else {
	     statements...
	 * } */
	/* Boolean expressions: expressions that can evaluate to either
	 * true or false.  For example, x == 3, or response != "yes".
	 * other (binary) boolean operators:
	 * ==
	 * !=
	 * <
	 * >
	 * <=
	 * >=
	 * There are also some unary operators, like !
	 * Furthermore, if given two boolean values, we have the
	 * following operators:
	 * && -- this gives the logical "and"
	 * || -- this gives the logical "or"
	 * */

	/* NOTE: any integer can also be interpreted as a boolean expression
	 * using the convention 0 <==> false, anything else <==> true.
	 * In our "Yes" example, the datatype of string constant "Yes" is
	 * a pointer (or memory address).  Since that memory address wasn't 0,
	 * it was treated as "true".
	 * */
	cout << "Enter an integer: ";
	int x;
	cin >> x;
	/* common mistake: using = when you meant ==. */
	if (x = 7) {
		cout << "You entered 7\n";
		// this will always happen.
		// TODO: note the compiler warning, and fix it.
	}
	/* NOTE: the assignment statement has the value of the RHS; in
	 * this case 7.  So, the cout statement will always happen.
	 * NOTE: there's actually a reason for assignment working this way:
	 * you can do stuff like this:
	 * x = y = z = 7; */
	/* NOTE: if you have ONLY ONE statement for the body,
	 * you can omit the braces.  Here is a nonsense example: */
	if (x == 12)
		cout << "you entered 12\n";
	else if (x == 13)
		cout << "you entered 13\n";
	else if (x == 14)
		cout << "you entered 14\n";
	else
		cout << "you entered something.\n";

	/* Loops. */
	/* Lets you execute a piece of code repeatedly until some
	 * condition is broken. */
	#if 0
	// an infinite loop:
	while (true) {
		cout << "@_@\n";
		/* If you write an infinite loop like this,
		 * you can break it with Ctrl-C. */
	}
	// NOTE: the "#if 0 ... #endif" is a convenient way to comment
	// large blocks of code (although that shouldn't be a very
	// common occurence...)
	#endif

	/* Something sensible: let's print out a table of squares.
	 * I.e., x*x for the first 10 positive integers.
	 * output should be 1,4,9,16,25... */
	int count = 1;
	while (count <= 10) {
		cout << count*count << " ";
		count++;
		/* NOTE: count++ is shorthand for "count = count+1" */
	}
	cout << endl;

	/* The above pattern of:
	 * 1. initialize some counter kind of variable
	 * 2. loop on the counter being below / above a bound
	 * 3. increment counter at the end of the loop
	 * is very common.  For these kinds of patterns, there is
	 * another loop that may be more suitable: the for loop.  */
	int i;
	for (i = 0; i < 11; i++) {
		cout << i*i << " ";
	}
	cout << i << endl; // TODO: make sure you understand why this prints 11

	/* TODO: write a piece of code that gets an integer from the user,
	 * and then prints the exponent (i.e. the base 2 logarithm) of
	 * largest power of 2 that goes into that integer.  For exzample,
	 * since 2 = 2^1, you should print 1.  For 4 = 2^2, the answer
	 * would be 2, for 24 = 2^3*3, the answer would be 3, and for
	 * 20 = 2^2*5, we want 2.  Hint: you should use the % operator,
	 * which gives you the remainder of a division.  */

	/* TODO: write a piece of code that reads integers from standard
	 * input until a negative integer is entered, and then computes
	 * the minimal value, the maximal value, and the average value
	 * of all the (non-negative) integers that were entered. */

	return 0;
}

// vim:foldlevel=2
