// Edgardo Molina
// CSC 103
// Lecture notes

#include <iostream>
using namespace std;

int main() {
    
    int a, b;
    string oper;
    
    cout << "Enter two numbers: ";
    
    // For cin don't assume that casting rules apply
    // Try inputing a double in this example 
    cin >> a >> b;
    

    cout << "Enter (a) for addition (b) for subtraction:"
        << endl; // code statements end with a semicolon. 
    // Line 21 is a continuation of line 20. 
        
    cin >> oper;
    
    if(oper == "a") {
        cout << a + b << endl;
    }
        
    if(oper == "b") {
        cout << a - b << endl;
    }
    
    // TODO: Add options for multiplication, division, and modulo
    
    /*
     *  Notice that in class I did not use curly braces for the if-statement
     *      body. Although that is allowed for one-line if-statements, I 
     *      recommend you get in the habit of always using them.
     *
     *  Try out the following and determine what happens:
     *      - Input doubles for the numbers
     *      - Input a string that is not "a" or "b" for oper
     *      - Input a capital "A" or "B"
     *      - Modify the code to accept either lowercase or uppercase oper
     */
    
    
    return 0;
}
