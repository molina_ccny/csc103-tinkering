/*
 * This file is not meant to be compiled.
 */

// ====================================================================
/*
 *  An example of integer addition.
 */
int x = 1;
int y = 2;

int z = x + y;

cout << "z: " << z << endl; // 3


// ====================================================================
/*
 *  Assigning real values to integers. The compiler automatically casts it
 *  to an int 
 */
int a = 1.9; // The value of a is 1
int b = 1.9;

int c = a + b;

cout << "c: " << c << endl; // 2


// ====================================================================
/*
 *  An example of integer division and modulo.
 */
int q = 3;
int w = 2;

int r = q / w;
int s = w / q;

cout << "r: " << r << endl; // 1
cout << "s: " << s << endl; // 0

int m = q % w;
int p = w % q;
cout << "m: " << m << endl; // 1
cout << "p: " << p << endl; // 2


// ====================================================================
/*
 *  An example of division with real numbers.
 */
double q = 3;
double w = 2;

double r = q / w;
double s = w / q;

cout << "r: " << r << endl;
cout << "s: " << s << endl;


// ====================================================================
/*
 *  An example of mixing integers and doubles. The compiler upgrades
 *  integers to doubles in expressions.
 */
double q = 3;
int w = 2;

int a = 1;
int b = 4;

int v = (q/w) + (a-b);

double r = q / w;
double s = w / q;

cout << "r: " << r << endl;
cout << "s: " << s << endl;


// ====================================================================
/*
 *  An example of integer addition.
 */
string h = "hello";

char c = 't';
char d = 'hello'; // WRONG - characters can only hold a single printable value.
char w = '\t', n = '\\';


