/*
 * This file is not meant to be compiled.
 */


// Assignment chaining and values 
int x, y, z;
z = y = x = 23;

// The Above happens left to right; 
// each statement evaluates to value 23 below.
// parens are for illustration purposes.
z = (y = (x = 23)); 

// Another example
int a, b;
a = b = 0;
// This statement evaluates to 0. Note that 0 is false in conditional expressions.


// General Format for loops
for( <initialization_list>; <condition>; <altering_list>) {
    <body>
}

// Example Loop
for(int i = -2; i < 10; i++) {
    cout << i << endl;
}

// Another example with an empty initialization statement
// and multiple alteration statements
int i = -2;
int j = 10;
for( ; i < 10; i++, j--) {
    cout << i << endl;
}

// semicolons are required
for(;;) { }


// shortcut operators
++i; // prefix version
i++; // postfix version
i = i + 1; // the above statements are the same as this

--i;
i--;
i = i - 1; // the above statements are the same as this

i += 10; // general form: i += <num>
i = i + 10; // same as: i = i + <num>;

// Other shortcut operators
+=
-=
*=
/=


