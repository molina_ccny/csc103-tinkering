/* More about loops.
 * See lectures 4,5,6 from Prof. Li.,
 * and chapters 3,4 in the book. */

// the usual stuff:
#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <string>
using std::string;
#include <cstdio> /* needed for printf and friends. */

int main()
{

	/* let's revisit an example from last time: computing the largest power
	 * of two that divides a given integer. */
	int n;
	cout << "enter an integer: ";
	cin >> n;
	/* 1. in English, what is the procedure?
	 *    repeatedly divide by 2, and count how many
	 *    times division worked w/o a remainder.
	 * 2. Add precision: what variables do you need?
	 *    Declare them all; note their significance;
	 *    make sure they have been initialized in a
	 *    sensible way.
	 *    We need perhaps the remainder and the count.
	 *    count should start at 0; remainder should be
	 *    ...the remainder.
	 * 3. Write + test.
	 *    */
	int nmod2 = n % 2;
	int count = 0; /* number of times (so far) 2 divided n */
	while (nmod2 == 0 && n != 0) {
		count++;
		n = n / 2; /* remove a 2 from the factors */
		nmod2 = n % 2;
	}
	cout << "number of 2's: " << count << endl;
	/* TODO: Re-write the above from scratch, and try to simplify it.
	 * Also, try to use a for loop instead of a while loop. */
	/* NOTE: another technique for writing loops:
	 * Each of your variables is supposed to have some meaning.
	 * Make sure that meaning is established before entering the loop.
	 * Then, on every iteration, make sure that meaning is *preserved*
	 * This is commonly refered to as "maintaining a loop invariant"
	 * */

	/* Read an arbitrary list of integers (until a negative is encountered)
	 * and then print the max, min, and mean. */

	cout << "enter a list of positive integers terminated by a negative.\n";
	cin >> n;
	if (n <= 0)
		return 0;
	int maxSoFar = n;
	int minSoFar = n;
	count = 1; /* we're reusing count from earlier. */
	int sumSoFar = n;
	do {
		cin >> n;
		if (n <= 0)
			break;
		if (n > maxSoFar)
			maxSoFar = n;
		if (n < minSoFar)
			minSoFar = n;
		count++;
		sumSoFar = sumSoFar + n;
	} while (n > 0);
	printf("Min: %i\nMax: %i\nAvg: %f\n",minSoFar,maxSoFar,((float)sumSoFar)/count);

	/* TODO: The above works, but parts of it are pretty ugly.  Rewrite it from
	 * scratch, and try to make it cleaner. */

	/* TODO: write code that gets an integer n from the user and prints out
	 * the n-th term of the fibonacci sequence. */
	/* TODO: write a loop that prints the sum of the first n odd cubes. */

	return 0;
}

// vim:foldlevel=1
