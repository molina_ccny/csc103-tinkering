// An example showing nested loops
// In C++ we are allowed to nest loops inside other loops
// as deep as we want.

// i.e.
// for(...) {
//     while(...) {
//         for(...) {
//             while() {
//                 //...
//             }
//         }
//     }
// }

#include <iostream>
#include <string>
using namespace std;

int main() {
    
    int countA = 0, countB = 0;
    
    while(countA < 5) {
        cout << "Value of countA is: " << countA << endl;
        for(countB=0; countB < 3; countB++) {
        // Comment out the above for loop and try the following alternatives
        // for(; countB < 3; countB++) {
        // for (countB = countA; countB < 5; countB++) {
            cout << "   Value of countB is: " << countB << endl;
        }
        countA++;
    }

    return 0;
}
