/* TODO: write a piece of code that reads integers from standard
 * input until a negative integer is entered, and then computes
 * the minimal value, the maximal value, and the average value
 * of all the (non-negative) integers that were entered. */
 
#include <iostream>
using namespace std; 
 
int main() {
 
    int mininput;
    int maxinput;
    double average = 0;
    int counter = 0;
    int input;
    
    cin >> input;
    
    mininput = input;
    maxinput = input;

    while(input >= 0) {
        counter++;
        average += input; // average = average + input;
        
        if(input < mininput) {
            mininput = input;
        }
        
        if(input > maxinput) {
            maxinput = input;
        }
        
        cin >> input;
    
    }
    
    cout << "The counter is: " << counter << endl;
    cout << "The min is: " << mininput << endl;
    cout << "The max is: " << maxinput << endl;
    cout << "The avg is: " << average/counter << endl;
}