/* TODO: write a piece of code that gets an integer from the user,
 * and then prints the exponent (i.e. the base 2 logarithm) of
 * largest power of 2 that goes into that integer.  For exzample,
 * since 2 = 2^1, you should print 1.  For 4 = 2^2, the answer
 * would be 2, for 24 = 2^3*3, the answer would be 3, and for
 * 20 = 2^2*5, we want 2.  Hint: you should use the % operator,
 * which gives you the remainder of a division.  */

/* TODO: write a piece of code that reads integers from standard
 * input until a negative integer is entered, and then computes
 * the minimal value, the maximal value, and the average value
 * of all the (non-negative) integers that were entered. */
 
#include <iostream>
#include <string>
using namespace std;

int main() {
    // ================================================================
    // Finishing up problem 2
    //      This version uses a do-while loop.

    int input = 0;
    int min = 0, max = 0;
    double sumSoFar = 0;
    int count = 0;

    // Prompt the user for the first number
    cout << "Enter pos integers: ";
    cin >> input;

    // The first number may be a negative!
    // In that case just end the program.
    if (input < 0) {
       cout << "No input received. Goodbye." << endl;
       return 0; // This ends the main function.
    }

    // If the first number is accepted, we initialize our variables.
    min = max = input;
    sumSoFar = input;
    count = 1; // One good number has been entered up to this point

    do {
        // ask for the next input
        cin >> input; 
   
        // If that input is negative we should end the loop
        if (input < 0)
            break; // break ends the loop
   
        // Add the input to the running sum.
        // This is also called accumulation.
        sumSoFar += input; // sumSoFar = sumSoFar + input;

        // One more is added to the count
        count++;
   
        // update max
        if(input > max) {
            max = input;
        }
   
        // update min
        if (input < min) {
            min = input;
        }
   
    } while( input >= 0 );
    // break takes me here.

    cout << "The max is: " << max << endl;
    cout << "The min is: " << min << endl;
    cout << "the avg is: " << (sumSoFar/count) << endl;
    
    
/*  
    // ================================================================
    // The following is the solution for problem 1.

    int input = 0;
    int result = 0;
    int rem = 0;
    int count = 0;

    cout << "Please enter a positive integer: ";
    cin >> input; // 11

    // if the number 0 or less, keep asking for correct input.
    while (input <= 0) {
        cout << "Please enter a positive integer: ";
        cin >> input;
    }

    result = input / 2; // 5
    rem = input % 2; // 1

    while((rem == 0) && (result != 0)) {
      count++;

      rem = result %  2;
      result = (result / 2);
    }

    cout << "The count is: " << count << endl;
*/
    
    return 0;
}
   
 
