// functions.
#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <string>
using std::string;

/* Functions give us yet another way to control the flow of execution of the
 * program. E.g., main() is a function that we've seen over and over... */

/* syntax:
 * datatype functionname(parameterlist) { c++ statements...}
 * think of f(x) from your calculus days... remember this notation?
 * f:R ---> R
 * Breaking it down, it says there is a function named f, which has domain R
 * and range R.  The C++ equivalent would be something like:
 * double f(double);
 * NOTE: thing above is called a "function prototype"
 */

// example: a function to compute x^4
// f(x) = x^4.
void f(int k) {
    cout << k << endl;
}

double f(double x) {
	// stuff to compute f goes here.
	return x*x*x*x;
}

// int f(string s) {
//     // n is the number of vowels in s;
//     // TODO
//     return n;
// }

// We have overloaded the function f

/* Terminology:
 * formal parameter -- this is the variable that is used
 * in the function's parameter list.
 * actual parameter -- this is the variable / value that you
 * actually called the function with.
 * E.g., in the above two functions, x was the formal parameter.
 * If I call them with byValueTest(a), then a is the actual parameter.
 *
 * For a call by value parameter, the formal parameter is a *copy* of
 * the actual parameter.
 * For a call by reference parameter, the formal parameter is a *synonym*
 * for the actual parameter.
 * */

/* example: write a function that computes
 * the nth term of the fibonacci sequence:
 * a_0 = 1; a_1 = 1; a_n = a_{n-1} + a_{n-2} */
unsigned int fibonacci(unsigned int n) {
	if (n < 2)
		return 1;
	//return fibonacci(n-1) + fibonacci(n-2);
	/* above will work, but it will be very slow.
	 * More on that later...  for now, let's do it
	 * without recursion. */
	/* what variables do we need? */
	unsigned int a0,a1;
	/* what is the meaning of a0 and a1? */
	/* a0 is the two spots back from n, and
	 * a1 is one spot back from n in the sequence. */
	a0 = a1 = 1;
	for (i = 2; i < n; i++) {
		/* TODO: finish this... */
	}
	/* TODO: try to clean up the entire function once you have
	 * something working. */
}

int main()
{
	/* now for how to use functions... */
	cout << "enter a number: ";
	double k;
	cin >> k;
	cout << "k^4 == " << f(k) << endl;
	cout << "k^4 == " << f(k+1) << endl;
	cout << "k^4 == " << f(k+2) << endl;
	cout << "k^4 == " << f(k) << endl;
	
	return 0;
}

// TODO: write a function that takes 3 integers and returns maximal value

// TODO: write a function that takes 3 doubles and returns the average value

// TODO: finish your homework, and convert that into a function called isPrime,
// which takes an integer and returns a boolean value indicating whether or not
// it is prime.

// TODO: write a function that takes two integer parameters and swaps the
// contents, i.e., if x=2 and y=5, then after calling swap(x,y), y=2 and x=5
// NOTE: for this, you will have to use *by reference* parameters.  Read
// up on that in Charles Li's lectures if needed.
