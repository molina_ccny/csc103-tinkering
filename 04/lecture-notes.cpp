#include <iostream>
using namespace std;


// q(x,y,z) = x^2 + y - z

// Multiple Variables
double q(double x, int y, double z) {
    return x*x + y - z;
}



// Function Prototype
//  this is used when you want to write your implementation
//  after its use in the main function or in any other function.
double f(double);
int h(int);
int w();


int main() {
    
    // function call
    cout << f(1.2) << endl;
    
    cout << (23 + 2) << endl;
    
    return 0;
}


// Function Implementation
// f(x) = x^3
double f(double x) {
    double cubed;
    
    cubed =  x*x*x;

    return cubed;
    
    // This line will never run. The function ends when the return statement is executed.
    cubed = 0;
}


// Call a function inside of another.
// the void is *optional* when the function has no parameters .
double g(void) {
    return f(2);
}

// A void return type is *required* when the function returns nothing.
void h(int y) {
    return;
}
