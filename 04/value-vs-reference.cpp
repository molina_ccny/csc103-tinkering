/* Testing by value vs by reference function calls. */

#include <iostream>
using std::cin;
using std::cout;
using std::endl;

/* by value: the formal parameters
 * are *copies* of the actual parameters. */
int byValueTest(int x) {
	x = 12;
	return x*x;
}

/* by reference: the formal parameters
 * are *synonyms* for the actual parameters. */
int byReferenceTest(int& x) {
	x = 12;
	return x*x;
}

int main()
{
	int y = 15;
	cout << "y == " << y << endl;
	byValueTest(y);
	cout << "y == " << y << endl;
	byReferenceTest(y);
	cout << "y == " << y << endl;
	return 0;
}
