// Edgardo Molina
// CSC 103

#include <iostream>
using namespace std;

int main() {
    
    int x, y;
    
    x = y = 10;
    
    // Don't do this
    // int z = x++ * --y + x*x*y--;
    
    // Post-fix 
    cout << x++ << endl;
    cout << y++ << endl;
 
    
    // Pre-fix
    cout << ++x << endl;
    cout << ++y << endl;
    
    // TODO: Run this code to understand the difference between pre- and post-fix.
    
    return 0;
}
