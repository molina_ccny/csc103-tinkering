Good Programming Practice
    - Use { } even with one line loops and if statements
        -- See what happens when you get careless:
            http://arstechnica.com/security/2014/02/extremely-critical-crypto-flaw-in-ios-may-also-affect-fully-patched-macs/
    - Indent your code correctly.
        -- makes it easier to debug code and visually check 


Vector (High Level)
    - think of it as a: container or collection or list
    - template class
        -- every element in the vector must be of the same type
        -- the same code is reused for different data types
    - declaration: vector<TYPE> variablename;
        -- initial vectors are empty
    - accessing the elements:
        -- we use the [ ] operator
    - order of entries are preserved
    - the first entry starts at position zero (0)
    - use the . (dot) operator to access class methods
        -- inputlist.size()
        -- inputlist.push_back(s)
    
    Example
        vector<string> inputlist;
        inputlist.push_back("hello");
        inputlist.push_back("world");
        inputlist.push_back("welcome:)");
  
        // our representation looks as follows:
        //    inputlist:
        //        0: "hello"
        //        1: "world"
        //        2: "welcome:)"
        // the vector contains 3 items at positions 0, 1, and 2.

        inputlist.size(); // the value is 3
  
        // Accessing elements:
        inputlist[1]; // the value is "world"
  
        // INVALID ACCESS
        inputlist[3]; // WHY IS THIS INVALID???


Arrays (Low Level)
    - We have to manage the size of the list.
    - We will go over it in a few lectures
    
    
TODO: Try the practice projects in vectors.cpp

