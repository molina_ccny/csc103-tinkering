// Introducing vectors; more on functions.

#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <string>
using std::string;
#include <vector>
using std::vector;

int main()
{
	/* problem:  read a list of strings from standard
	 * input until you find a string with "_END_".
	 * Then print the list back out in reverse order.
	 * The issue is that we don't know ahead of time how
	 * many variables we'll need...  Vectors are one answer. */
	string s;
	vector<string> inputlist;
	/* NOTE: the vector is not actually a datatype!  Vector
	 * is a *template* which is like machine that takes in
	 * an existing datatype T, and makes a new datatype which
	 * can store a list of T's.  */
	cout << "enter a list of strings terminated by _END_.\n";
	while (cin >> s && s != "_END_") {
		inputlist.push_back(s); // adds s to end of the list
	}
	// now we have all the strings... how to access them??
	// use the square bracket [] notation to access elements;
	// the size() function tells us... the size.
	for (int i = inputlist.size() - 1; i >= 0; i--) {
		cout << inputlist[i] << endl;
		/* The pop_back() function returns a void. the following
		 *  would remove the last element and empty the vector.
		 */
        //inputlist.pop_back(); 
	}
	
	for (int i = 0; i < inputlist.size(); i++) {
		cout << inputlist[i] << endl;
	}
	
	
	/* Other vector stuff: you can remove from the end
	 * with the pop_back() function. */

	/* another example: read two lists of strings, and then
	 * shuffle them together, appending leftover elements
	 * of the longer one. */
	vector<string> L1;
	vector<string> L2;
	/* first read L1 (use _END_ again for the end of input) */
	while (cin >> s && s != "_END_") {
		L1.push_back(s);
	}
	
	// while(cin >> s) {
	//         if(s == "_END_"){
	//             break;
	//         }
	//     }
	//     
	while (cin >> s && s != "_END_") {
		L2.push_back(s);
	}
	for (size_t i = 0; i < L1.size() || i < L2.size(); i++) {
		if (i < L1.size())
			cout << L1[i] << endl;
		if (i < L2.size())
			cout << L2[i] << endl;
	}
	/* NOTE:  the above code is correct, but there may be a subtle performance
	 * issue (don't worry about this much / or at all), which is that the above
	 * code is not friendly to the processor's branch prediction algorithms.
	 * More reading, in case you're interested:
	 * https://en.wikipedia.org/wiki/Branch_predictor */

	/* TODO: try to re-write, starting with this: */
	//for (size_t i = 0; i < L1.size() && i < L2.size(); i++) {

	/* TODO: once you're done, convert what you wrote into a *function* that
	 * takes two vectors of strings, and returns a vector with the strings
	 * shuffled.  Remember: you have to write it OUTSIDE of main()... */
	/* TODO: write test code for the function you just wrote. */

	/* TODO: write code that *intentionally* accesses vector elements
	 * which are out of bounds.  E.g., try to access V[15] when the
	 * vector only has 10 items.  Nothing will prevent this from compiling,
	 * but how far out of bounds do you have to go before your program
	 * crashes?  When it does crash, take note of the error messages;
	 * this will unfortunately not be the last time you see them : )  */
	return 0;
}
