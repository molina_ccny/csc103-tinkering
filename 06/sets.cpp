// working with sets and maps.
#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <set>
using std::set;
#include <string>
using std::string;
#include <cstdio>


// example 1: write a function that reads strings from standard input
// stores them in a set, and returns the number of strings read (which
// may be different from the size of the set...).
int readStrings(set<string>& S) { // NOTE: by reference is important here
	string input;
	int count = 0;
	while (cin >> input) {
		S.insert(input);
		count++;
	}
	return count;
}

/* TODO: emulate the insert function for the set, but for a vector.
 * That is, write a function that takes a vector (say of integers)
 * and a single integer x, and adds x to the vector *only if it was
 * not already present*. You can return a boolean indicating whether
 * or not x insertion took place. */

/* TODO: write a function that removes a value from a vector if it
 * is present.  It should take a vector and a value x, and remove
 * x if it is in the vector. NOTE: you don't have to preserve the
 * order of the other elements. */

int main()
{
	/* NOTE: it is not entirely clear how to print out
	 * the contents of a set.  Here's one way to do it: */
	set<string> T;
	
	int total = readStrings(T);
	
	
	printf("Total:\t%i\nUnique:\t%i\n",total,T.size());
	
    cout << "Total: " << total << endl;
    cout << "Unique: " << T.size() << endl;
    
	
	for (set<string>::iterator i = T.begin(); i!=T.end(); i++) {
		cout << *i << endl;
	}
	
	
	return 0;
}
