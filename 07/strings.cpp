/* strings.cpp
 * More examples working with strings...
 * */
#include <iostream>
using std::cin;
using std::cout; 
using std::endl;
#include <string>
using std::string;


/* Example 1: write a function that takes a string and a character
 * and returns the number of occurences of the character in the string.
 * */

/* NOTE: const by-reference is used to fake a by value call.
 * (More efficient...) */
int countChars(const string& s, char c = 'a')
{
	// s = "hahahah"; /* TODO: decipher error messages from this line. */
    
	int count = 0;
	for (size_t i = 0; i < s.length(); i++) {
		if (s[i] == c) 
			count++;
	}
	return count;
}

/* TODO: write a function that takes two strings and returns an
 * integer value indicating whether or not the first was a substring
 * of the second; the integer should be the index at which the string
 * was found, or -1 to indicate that the string was not found.
 * For example, findSubstr("def", "abcdef") would return 3.
 * */

/* NOTE: there is a built-in string function for this (find(str,pos)).
 * TODO: try it out.
 * */

/* TODO: write a function that takes a string and returns a boolean
 * indicating whether or not it was a palindrome. Example: "racecar"
 * */ 

/* TODO: write a function that takes a string and returns another
 * string, which is the input reversed.
 * */ 
 

int main() {
	/* TODO: write test code for all of the functions you wrote above */
	string t;	
	cout << "enter a string: ";
	getline(cin,t);
	cout << "enter a character: ";
	char c;
	cin >> c;
	cout << countChars(t,c) << endl;
    cout << "The value of t is: " << t << endl;
    cout << countChars(t) << endl;
    cout << "The value of t is: " << t << endl;
	return 0;
}





