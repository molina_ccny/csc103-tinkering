// arrays and pointers.
#include <iostream>
using std::cin;
using std::cout;
using std::endl;

int main() {
	/* Arrays in C/C++: kind of like a "dumb" version of std::vector.
	 * It has no concept of its size.  It has no fancy member functions
	 * like push_back, etc... */

	/* Example 1: declare an array of 10 integers, fill it with squares
	 * of the integers 0..9 */
	int A[10]; // just like normal declaration, but with [10]
	
	/* Let's print out the values stored in the array immediately after
	 * declaring it ... you should notice that some elements have values.
	 * These values are sometimes refered to as "garbage or junk" data.
	 * These values were placed there by the program that was previously
	 * using that memory. 
	 */
	for (size_t i = 0; i < 10; i++) {
		cout << A[i] << " ";
	}
    cout << endl;
	
	/* initialize the array with some values
	 */
	for (size_t i = 0; i < 10; i++) {
		A[i] = i*i;
	}
	
	/* print the values in the array again
	*/
	for (size_t i = 0; i < 10; i++) {
		cout << A[i] << " ";  
	}
    cout << endl;
    
	/* what's in this array?  Well, consecutive squares.  But what
	 * really is in the symbol "A"? */
	cout << "A == " << A << "\n";

	/* Woah... just prints a memory address.  But that's all A is!!
	 * The address tells us where the array starts; i.e., the location
	 * of A[0].  The other elements are in adjacent addresses. */

	/* what does this A[i] notation really mean???
	 * A[i] really just means, take the memory address of A, add
	 * i, and read the memory contents at that location.
	 * There are some strange consequences of this: */
#if 0
	for (size_t i = 0; i < 10; i++) {
		cout << i[0] << endl;
	}
#endif

	/* POINTERS...
	 * these are important.  They are just *variables that hold a
	 * memory address*.  E.g., our array A is a pointer.  What kinds of
	 * things might we want to do with pointers?  Well the obvious ones
	 * are:
	 * 1.  declare them
	 * 2.  initialize them
	 * 3.  read and write from the address stored in the pointer.
	 * */

	// 1. how to declare a pointer:
	int* p;  // note the *.  int* is a new datatype, called "pointer to int"
	// NOTE: we could also declare as int *p; which as we'll see, indicates
	// that *p is an integer.
	// 2.  how to initialize?  Well, you could just type in an address
	// p = 23452345; // usually this is a bad idea.  however...
	p = 0; // this is common.  The 0 address is never yours, so we can
		   // use it to distinguish good addresses from bad.  Often you
		   // will see "NULL" used as well.  This is a synonym for 0.
	int x = 17;
	// we can find out where x lives like this:
	p = &x;  // new stuff: usage of the ampersand.  If x had datatype blah,
	         // then &x is of type blah*

	cout << "p == " << p << "\tx == " << x << endl;
	// 3. how to read and write them??
	cout << "this is what's at the address stored in p: " << *p  << endl;
	// note the star operator.  AKA "dereference operator"
	(*p)++;
	cout << "p == " << p << "\tx == " << x << endl;

	int y = 23;
	int* q = &y;
	cout << "p == " << p << "\tx == " << x << endl;
	cout << "q == " << q << "\ty == " << y << endl;
	p = q;
	(*q)++;
	cout << "p == " << p << "\tx == " << x << endl;
	cout << "q == " << q << "\ty == " << y << endl;

	// NOTE: after the p = q statement, p is pointing to the same
	// thing as q; i.e., y.  So now (*p), (*q), y are all synonyms.

	/* were those parens in (*p)++ important?  Yes. */
	cout << "~~~~~~~~~~~~~~~~~~~~~~~~~\n";
	cout << "p == " << p << "\t*p == " << *p << endl;
	*p++;
	cout << "p == " << p << "\t*p == " << *p << endl;



    /* print the values in the array again, 
     * this time using pointer math.
	 */
    int* w;
    w = A;
	for (size_t i = 0; i < 10; i++) {
		cout << *w << " ";
        w++; // moving the pointer
	}
    cout << endl;
    /* Try running the loop for more than 10 iterations.
     * What happens? Why is this wrong and potentially harmful?
     */


	return 0;
	// TODO: declare another pointer (say p2) to a character, initialize it
	// just as we did before, and print out (p+i) and then (p2+i) for small
	// values of i. Notice that the difference in memory addresses changes.
}

/* TODO: write a function that returns void, and takes two
 * POINTERS to integers, and swaps the contents of those two
 * memory locations.  Note: part of this exercise is understanding
 * what I'm asking for...
 * */

/* TODO: write a function that performs a "circular shift" on
 * an array of integers.  For example, if the input array
 * contained 0,1,2,3,4 and we shifted by 2, the new array would
 * contain 3,4,0,1,2.
 * NOTE: you can do this with *a constant amount of additional storage*.
 * That is, irrespective of the size of the array, your function should
 * allocate the same, fixed amount of storage.  No vectors.  No additional
 * arrays.  Just a few integers.  Try it!
 * */

