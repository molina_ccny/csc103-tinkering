Arrays
    -- Low level
    -- sequence of memory
    -- declaration
      TYPE variablename[size];
      
      int A[10];
      
    -- have fixed size
    -- programmer has to keep track of the size
    
Pointers
    -- the values of pointers are memory addresses
    -- we can inspect the contents at a memory address using the dereferencing operator *
     
    int x = 2;
    int* p = &x;

    -- & is the "address of" operator
    -- (&x) returns the memory address of variable x.
    
    (int*) is a type for a pointer to an int.
    
    double* p2;
    
    (double*) is a type for a pointer to a double.
    
Link to Binky video:
http://cslibrary.stanford.edu/104/
Click on the C++ version.
    
    
    
    
    
    
    