/* here are a few extra things for you to try, in case
 * you didn't get enough practice with sets */

// TODO: write a main program that reads two sets of strings
// from standard input.  you can read until you get a string
// equal to "__END" or something, if you want.   Once you've
// read the sets, print them back out to standard output.

// TODO: write a function that takes two sets, and returns a
// 3rd set which contains the union of the two parameters.

// TODO: write a function that takes two sets, and returns a
// 3rd set which contains the intersection of the two parameters.

// TODO: write code to test the functions you wrote.

#include <iostream>
#include <set>
#include <string>
using namespace std;


set<string> setunion(set<string> a, set<string> b) {
    
    // The final set containing the union of a and b
    set<string> c;
    
    set<string>::iterator i;
    
    // Add the items of set a into set c.
    for(i = a.begin(); i != a.end(); i++) {
        c.insert(*i);
    }
    
    /* The following would be more efficient than using the loop above. */
    // c = a;
     
    // Add the items of set b into set c.
    for(i = b.begin(); i != b.end(); i++) {
        c.insert(*i);
    }
    
    // return set c.
    return c;
}


set<string> setintersection(set<string> a, set<string> b) {
    
    set<string> c;
    
    set<string>::iterator i;
    set<string>::iterator j;
    
    // Method 1: For every element in a, check if it is equal to an element of b 
    // for(i = a.begin(); i != a.end(); i++) {
    //         for(j = b.begin(); j != b.end(); j++) {
    //             if( *i == *j ){
    //                 c.insert(*i);
    //                 break;
    //             }
    //         }
    //         
    //     }
    
    // Method 2: Using the find() function improves performance
    for(i = a.begin(); i != a.end(); i++) {
        if(b.find(*i) != b.end()) {
            c.insert(*i);
        }
    }
    
    
    return c;
}




int main() {
    
    string input;
    set<string> s1;
    set<string> s2;
    
    cout << "Please enter set 1 (Enter '__END' to stop): " << endl;
    while(cin >> input && input != "__END") {
        s1.insert(input);
    }
    
    cout << "Please enter set 2 (Enter '__END' to stop): " << endl;
    while(cin >> input && input != "__END") {
        s2.insert(input);
    }
    
    
    set<string>::iterator i;
    for(i = s1.begin(); i != s1.end(); i++) {
        cout << *i << endl;
    }
    
    cout << endl << endl;
    
    for(i = s2.begin(); i != s2.end(); i++) {
        cout << *i << endl;
    }
    
    
    return 0;
}











