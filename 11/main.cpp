#include "rectangle.h"
#include <iostream>
/* NOTE: double quotes instead of brackets:  this
 * controls the default search path when the compiler
 * is figuring out where rectangle.h lives.  Quotes
 * will instruct it to look in the current directory
 * first before moving on to /usr/include/... */

/* TODO: run `make clean` to erase the binaries, and then compile
 * this program manually using g++.  Run g++ -c first to produce
 * object files, and then run g++ again to link them into an executable. */

int main(void)
{
    
	rectangle R(8,15);
	std::cout << "Area of R is " << R.area() << "\n";
	R.draw(); // NOTE: there's a hidden parameter in these function calls.
			  // the "this" pointer gives us access to whose member function
			  // is being called...
			  
	
	/* When width and length are public
     * We can do the following: */
    //R.width = 10;
    //R.height = 20;
    /* Typically, we prefer to make class data members private.
     * And changes to the data members should be controlled by
     * class member functions. */
    
    
	return 0;
}
