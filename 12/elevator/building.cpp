#include <iostream>
using namespace std;

#include "building.h"
#include "elevator.h"

// ==================================================================
// Building Method Implementation


Building::Building(int numFloors) {
    _numFloors = numFloors;
    el_lobbyA.setMaxFloor(_numFloors);
    el_lobbyB.setMaxFloor(_numFloors);
    
    cout << "===== BUILDING CREATED =====" << endl;
    cout << "A new bulding has gone up with " << _numFloors << " floors." << endl;
}



void Building::drawBuilding() {
    cout << "  -----" << endl;
    for (int i=_numFloors; i > 0; i--) {
        char A = (el_lobbyA.isAt(i) ? 'A' : ' ');
        char B = (el_lobbyB.isAt(i) ? 'B' : ' ');
        
        // // The 2 lines above are shorthand for the following assignment and if-else statement (x2)
        // char A;
        // if (el_lobbyA.isAt(i))
        //     A = 'A';
        // else
        //     A = ' '
        
        cout << i << " |" << A << "|" << B << "|" << endl;
    }
    cout << "  -----" << endl;
}



Elevator& Building::callElevator(int fromFloor) {
    // call nearest elevator
    int distA = abs(fromFloor - el_lobbyA.getFloor());
    int distB = abs(fromFloor - el_lobbyB.getFloor());
    
    if( distA <= distB) {
        el_lobbyA.request(fromFloor);
        return el_lobbyA;
    } else {
        el_lobbyB.request(fromFloor);
        return el_lobbyB;
    }
}

