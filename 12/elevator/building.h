#ifndef BUILDING_H
#define BUILDING_H

#include "elevator.h"

// ==================================================================
// Building Class Declaration

class Building {

    public:
        Building(int numFloors = 12);
        
        Elevator& callElevator(int fromFloor);
        void drawBuilding();
        
    private:
        Elevator el_lobbyA;
        Elevator el_lobbyB;
        
        int _numFloors;
};

#endif
