#include <iostream>
using namespace std;

#include "elevator.h"

// ==================================================================
// Elevator Method Implementations


Elevator::Elevator(int numFloors) {
    _currentFloor = 1;
    _maxFloor = numFloors;
}



bool Elevator::isAt(int floor) {
    return floor == _currentFloor;
}



void Elevator::request(int newfloor) {
    // TODO: Add checks for invalid newfloor
    
    if (newfloor == _currentFloor)
    {
        cout << "Elevator is here.";
    }
    else if ( newfloor > _currentFloor)  // move elevator up
    {
      cout << "\nStarting at floor " << _currentFloor << endl;
      while (newfloor > _currentFloor)
      {
        _currentFloor++;    // add one to current floor
        cout << "   Going Up - now at floor " << _currentFloor << endl;
      }
      cout << "Stopping at floor " << _currentFloor << endl;
    }
    else  // move elevator down
    {
      cout << "\nStarting at floor " << _currentFloor << endl;
      while (newfloor < _currentFloor)
      {
        _currentFloor--;   // subtract one from current floor
        cout << "   Going Down - now at floor " << _currentFloor << endl;
      }
      cout << "Stopping at floor " << _currentFloor << endl;
    }
}

