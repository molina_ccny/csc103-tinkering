#ifndef ELEVATOR_H
#define ELEVATOR_H

// ==================================================================
// Elevator Class Declaration

class Elevator {
    
    public:
        Elevator(int numFloors = 10);
        bool isAt(int floor);
        void request(int newfloor); 
        int getFloor() { return _currentFloor; } // inline methods
        void setMaxFloor(int maxFloor) { _maxFloor = maxFloor; } // inline methods
        
    private:
        int _currentFloor;
        int _maxFloor;
};

#endif
