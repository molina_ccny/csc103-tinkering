// Modeling a building with 2 elevators using classes in C++

#include <iostream>
using namespace std;

#include "building.h"
#include "elevator.h"


// ==================================================================
// Main Function

int main() {
    
    // TODO: When are the Elevator objects created?
    Building building(8);
    int callFloor, destFloor;

    building.drawBuilding();

    while(true) {
        cout << "Which floor are you on: ";
        cin >> callFloor;
        cout << "Going to: ";
        cin >> destFloor;

        // TODO: Why is el a reference?
        Elevator& el = building.callElevator(callFloor);
        building.drawBuilding();
        el.request(destFloor);
        building.drawBuilding();
    }

    return 0;
}
