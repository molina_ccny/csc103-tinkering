#include <iostream>
using namespace std;

/* A simple struct defining a point that has x and y values
 * this struct is defining a new user-defined datatype
 */
struct point {
    int x;
    int y;
};


struct point p1;
struct point p2;

/* Declaring variables using the above datatype is done as follows: */
// struct point myPoint;
/* The complete datatype above is "struct point". */


/* Since typing "struct point" is verbose, we can use the typedef feature
 * to define a new (shorter) name for it: */
typedef struct point Point;


/* This allows us to now declare point objects as follows: */
// Point P, Q;


/* Here is another typedef example that really saves some typing */
typedef unsigned long long int ULLint;

ULLint count, i;

/* Here is another way of combining the typedef with the struct definition */ 
// typedef struct {
//    int    x;
//    int    y;
// } point;
// 
// typedef point Point;
// typedef struct point Point; // this is wrong in the second version

int main() {
    
    // using our newly defined struct and typedef
    Point p;
    ULLint a, b;
    
    // this is a bug! Why?
    cout << p.x << ", " << p.y << endl;
    
    // Assigning values to the struct members
    p.x = 10;
    p.y = 12;
    
    //p.x = p.y = 10;
    
    cout << p.x << ", " << p.y << endl;
    
    return 0;
}
