// Edgardo Molina
// CSC 103

#include <iostream>
using namespace std;

int main() {
    
    // A normal loop printing integers
    cout << "A list of numbers: " << endl;
    for(int i=32; i < 127; i++) {
        cout << i << " ";
    }
    cout << endl << endl;
    
    
    // The PRINTABLE ascii characters
    cout << "All printable characters: " << endl;
    for(char c=32; c < 127; c++) {
        cout << c << " ";
    }
    cout << endl << endl;
    
    
    // Since char's have numeric representations
    //  we can increment the variable 'c', while
    //  still assigning it character values. 
    cout << "A list of characters: " << endl;
    for(char c = 'A'; c <= 'Z'; c++) {
        cout << c << " ";
    }
    cout << endl << endl;
    
 
    return 0;
}








    // for(char c = 32; c < 127; c++) {
    //     cout << c << endl;
    // }
    // 
    // for(char c = 'a'; c <= 'z'; c++) {
    //     cout << c << endl;
    // }