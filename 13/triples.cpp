
#include <iostream>
using namespace std;

int main() {
    
    /*
     * The following piece of code shows how to enumerate all possible doubles
     * up to some bound. In homework 4 you will need to do something similar but
     * instead enumerate all possible triples.*/
    
    // TODO: Extend to enumerate all possible triples.
    int bound = 40;
    for (int a=1; a <= bound; a++ ) {
        for (int b = 1; b <= bound; b++) {
            cout << a << "," << b << ", " << endl;
            
        }
    }
    
    return 0;
}
