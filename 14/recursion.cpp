#include<iostream>
using std::cin;
using std::cout;
using std::endl;

// what's the output of printStuff(3)?
void printStuff(unsigned long x)
{
	if(x == 0) {
		cout << "We stopped at x == 0\n";
	} else {
	    cout << "Calling printStuff(" << x << ")\n";
		printStuff(x-1);
		cout << "Ending printStuff(" << x << ")\n";
		
		// TODO: swap the two lines above, and take note of the difference
		// in the output that's produced.
	}
}

// TODO: write a recursive function that prints the base 10 digits of n
// vertically to cout.  for example, printVertically(2358) would print
// 2
// 3
// 5
// 8
// Rules: you can't use any loops.  You can't use vectors or arrays.
// Just let the recursive function calls do the work for you.
void printVertically(unsigned long n)
{
	// your code goes here.
}

int main(int argc, char** argv)
{
	// test out the functions...
	printStuff(3);
	return 0;
}
