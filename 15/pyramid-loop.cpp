// Edgardo Molina
// CSC 103
// Printing a pyramid with a loop

#include <iostream>
using namespace std;

int main() {
    
    // Ask the user for the height of the pyramid
    int h;
    cin >> h;
    
    // A counter for each row/line of the pyramid
    for(int r=0; r < h; r++) {
        
        // print spaces followed by stars
        for(int s=h-r; s > 0; s--) {
            cout << "-"; // added a - to see what we were doing.
        }
    
        // Printing the stars
        for(int s=0; s < (2*r + 1); s++) {
            cout << "*";
        }
    
        // finally end the line
        cout << endl;
        
    }
    
    
    return 0;
}
