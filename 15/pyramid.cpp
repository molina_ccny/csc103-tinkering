
#include <iostream>
using namespace std;

void pyramid(int spaces, int stars = 1) {
    if(spaces == 0) {
        return;
    } else {
        for(int i=0; i<spaces; i++ ) { cout << " "; }
        for(int i=0; i<stars; i++ ) { cout << "*"; }
        cout << endl;
        pyramid(spaces-1, stars+2);
        
        for(int i=0; i<spaces; i++ ) { cout << " "; }
        for(int i=0; i<stars; i++ ) { cout << "*"; }
        cout << endl;
        
        return; 
    }
}


int main() {
    int height;

    cout << "Enter a height: ";
    cin >> height;

    pyramid(height);

    return 0; 
}