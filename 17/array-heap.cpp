
#include <iostream>
using namespace std;

int main() {
    
    int n;
    
    cout << "please enter a number n: ";
    cin >> n;
    
    int* a = new int[n]; 
    
    for(int i=0; i<n; i++) {
        a[i] = i*i;
    }
    
    for(int i=0; i<n; i++) {
        cout << a[i] << " ";
    }
    
    cout << endl;

    
    return 0;
}
