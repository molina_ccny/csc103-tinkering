// Arrays on the Stack

#include <iostream>
using namespace std;

int main() {
    
    int size = 10;
    
    int A[10];
    
    
    for(int i=0; i<size*2; i++) {
        A[i] = i*i;
    }
    
    for(int i=0; i<size; i++) {
        cout << A[i] << " ";
    }
    cout << endl;
    
    
    return 0;
}
