// A dynamically growing array example


#include <iostream>
using namespace std;


#define DEBUG 1

// pay attention to the arrays type: int*&
// We are passing an integer pointer as a parameter,
// which is itself being passed by reference.
void expand(int*& A, int cursize, int newsize) {
    
    // (1) Allocate a new array B
    int* B = new int[newsize];
    
    // (2) Copy the contents of A into B
    for(int i=0; i<cursize; i++) {
        B[i] = A[i];
    }
    
    // (3) Release the old array A
    delete[] A;
    
#if DEBUG
    // (3.5) Some debug statements
    cout << "expand() function was called..." << endl;
    cout << "A: " << A << endl;
    cout << "B: " << B << endl;
#endif

    // (4) Return the new array, by reassigning pointer A to point to
    //     new array B. Notice that pointer A is passed by reference.
    A = B;
    
}


int main() {
    
    int x;
    int capacity = 3;
    int size = 0;
    
    int* nums = new int[capacity];
    
    
    while(cin >> x) {
        
        if(size == capacity) {
            capacity *=2;
            expand(nums, size, capacity);
        }
        
        nums[size] = x;
        size++;
    }
    
    // Don't forget to always free up your memory.
    delete[] nums;
    nums = NULL;

    return 0;
}
