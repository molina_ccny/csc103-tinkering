#include <iostream>
using std::cin;
using std::cout;
using std::endl;

#include "vector2.h"

int example()
{
	vector2 V;
	V.push_back(199);
	V.push_back(2929);
	return 0;
} /* at this point, V is going out of scope. ~vector2() will be called. */

int main()
{
	/* TODO: write more test code to see if your vector really works
	 * the way you expect. */
	
    
	vector2 V;
	
    cout << V[80] << endl;
	/* The above is accessing the array out of bounds, but
	 * most likely, your program isn't crashing, since the
	 * address will still be in the programs segment of memory.
	 * Go back to vector2.cpp and enable assertions, compile
	 * again, and see what happens. */
	 
     example();
	 
	return 0;
}
