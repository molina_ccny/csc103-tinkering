/* implementation file for our vector class. */
#include "vector2.h"

/* this will disable assertions: */
#define NDEBUG 1
/* NOTE: this is much better done from a makefile with -D
 * passed to gcc.  I'm putting it here so it is easier
 * for you to mess with. */

#include <cassert> /* for assert() */

vector2::vector2(size_t initSize)
{
	/* must establish class invariant. */
	this->size = 0;
	this->capacity = initSize;
	this->data = new int[initSize];
}

vector2::~vector2() // destructor.  <--- NEW STUFF!
{
	/* called automatically whenever an instance:
	 * (a) goes out of scope, or
	 * (b) is explicitly deleted */
    delete[] this->data;
}

void vector2::push_back(int x)
{
	/* remember, this has to work no matter what (as long as the
	 * machine still physically has memory available) */
	if (this->size < this->capacity) { /* easy case. */
	    this->data[size++] = x;
		
		/* The line above is the same as the following two lines. */
        // this->data[size] = x;
        // size++;
		
	}
	else /* hard case... */
	{
		/* expand data by a factor of 2. */
		int* temp = new int[capacity*2];
		/* now copy... */
		for (size_t i = 0; i < size; i++) {
			temp[i] = this->data[i];
		}
		temp[size++] = x;
		delete [] this->data;
		this->data = temp;
        temp = NULL;
	}
	/* TODO: try to make this shorter by growing the array
	 * first, if needed.  */
}



/* TODO: write a corresponding "pop_back" function that removes
 * the last element of the vector. */

int vector2::operator[](size_t i) {
	assert(i<size); /* if a preprocessor symbol NDEBUG is
					   defined, this does nothing. */
	return this->data[i];
	/* will V[0] = 10; work???  Find out. */
}

/* TODO: browse the makefile if you have time.  It is slightly
 * more interesting than the ones that came before it. */



/* TODO: We are free to overload any of the C++ operators
    for the vector2 class. For example, we can overload the
    + operator as follows. But what should it do? This is 
    up to you as the designer of the vector2 class. Try 
    implementing a + operator which appends RHS to LHS and
    returns it as a new vector2 object. */
// vector2 vector2::operator+(vector2 LHS, vector2 RHS) {
// 
// }


