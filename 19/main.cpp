#include <iostream>
using std::cin;
using std::cout;
#include "vector2.h"

void fun1(vector2 W)
{
	cout << "This goes wrong without a proper copy constructor\n";
	return;
}

void fun2(vector2& W)
{
	vector2 v2;
	cout << "This will go wrong without an assignment operator x_x\n";
	v2 = W; // This will call the assignment operator!
	return;
}

int main()
{
	vector2 V1;
    vector2 V2;
    vector2 V3;
	
    V1.push_back(12);

    vector2 Q(V1); // calls the copy constructor
    vector2 A = Q; // call the assignment operator

    vector2 V;
	
	fun1(V); // passes V by value - this creates a copy with copy constructor
	fun2(V); // passes V by reference - no problem yet
	return 0;
}
