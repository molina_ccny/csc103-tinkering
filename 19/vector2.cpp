/* implementation file for our vector class. */
#include "vector2.h"

/* this will disable assertions: */
#define NDEBUG 1
/* NOTE: this is much better done from a makefile with -D
 * passed to gcc.  I'm putting it here so it is easier
 * for you to mess with. */

#include <cassert> /* for assert() */

vector2::vector2(size_t capacity)
{
	/* must establish class invariant. */
	this->size = 0;
	this->capacity = capacity;
	this->data = new int[capacity];
}

/* TODO: make sure you understand why we need this. */
// Deep Copy
vector2::vector2(const vector2& V)
{
	/* have to make *this a real copy of V. */
	this->capacity = V.capacity;
	this->size = V.size;
	this->data = new int[capacity];
	for (size_t i = 0; i < size; i++) {
		this->data[i] = V.data[i];
	}
}

// DEFAULT behavior
/*
vector2::vector2(const vector2& V)
{
	this->capacity = V.capacity;
	this->size = V.size;
    this->data = V.data; // Pointing to the same data array!!
}
*/


/* TODO: make sure you understand why we need this. */
vector2& vector2::operator=(const vector2& RHS)
{
	/* NOTE: LHS (*this) already exists, in contrast with the
	 * copy constructor.  Hence... have to erase the LHS's data
	 * before re-assigning. One subtle point: what if RHS *is* LHS??
	 * E.g., if someone does V = V;  */
	if (this == &RHS) return *this; /* check for self-assignment */
	/* Now it's safe to erase the LHS array: */
	delete [] this->data;
	/* then proceed as in the copy constructor: */
	this->capacity = RHS.capacity;
	this->size = RHS.size;
	this->data = new int[capacity];
	for (size_t i = 0; i < size; i++) {
		this->data[i] = RHS.data[i];
	}
	return *this;
}



vector2::~vector2()  /* destructor. */
{
	/* called automatically whenever an instance:
	 * (a) goes out of scope, or
	 * (b) is explicitly deleted */
	delete[] this->data;
}

void vector2::push_back(int x)
{
	if (this->size >= this->capacity) {
		/* expand data by a factor of 2. */
		capacity *= 2;
		int* temp = new int[capacity];
		for (size_t i = 0; i < size; i++)
			temp[i] = this->data[i];
		delete [] this->data;
		this->data = temp;
	}
	this->data[size++] = x;
 
}

/* TODO: write a pop_back() function. */
int vector2::pop_back() {
    
    return 0;
}


/* NOTE: returning *by reference* */
int& vector2::operator[](size_t i)
{
	assert(i<size); /* if a preprocessor symbol NDEBUG is
					   defined, this does nothing. */
	return this->data[i];
}
