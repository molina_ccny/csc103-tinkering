/*
 * We'll try to implement our own vector class, and see what is
 * going on under the hood.  What you should be learning from all this:
 * 1. more about classes
 * 2. dynamic memory management
 */
#pragma once

#include <cstddef> // for size_t definition.

/* TODO: fill out more of the interface of a STL vector.  We're
 * missing things like reserve(), shrink_to_fit(), clear()...
 * More ideas can be found here:
 * http://www.cplusplus.com/reference/vector/vector/ */

class vector2 {
	public:
	/* stuff in this section is declared as "public" which means that it is
	 * part of the public interface -- i.e., the part of our code that other
	 * programmers are supposed to use. */

	/* constructors: */
	vector2(size_t initSize=64); // default constructor
	/* could call via vector2(somevalue) or just vector2(); */
	/* Copy constructor:  New stuff. */
	vector2(const vector2& V); /* NOTE: parameter must be by reference,
							since the copy constructor is the thing
							that defines what by value does. */
	/* NOTE: copy constructor will be called "behind scenes" whenever
	 * you pass a parameter by value, or return by value, or for some
	 * intermediate computations.  If you don't define a copy constructor,
	 * the default one will just do member-wise assignment.  E.g.,
	 * this->size = V.size; this->capacity = V.capacity; this->data = V.data; */
	/* destructors:  <--- NEW STUFF! */
	~vector2(); /* note the syntax: no return value; no params; same name. */
	/* member functions: */
	void push_back(int x);
    int pop_back();
	int& operator[](size_t i);
	/* assignment operator: */
	vector2& operator=(const vector2& RHS);
	/* NOTE: *this is the implicit LHS.  That is,
	 * W = V;  <====>  W.operator=(V); */
	/* NOTE: the reason the return type is by reference is so that
	 * people can chain assignments, e.g. V = W = Z; */

	private:
	/* stuff below this line is declared as "private" meaning that these
	 * members are considered to be implementation details.  No user of our
	 * class should be concerned with it.  The compiler will in fact prevent
	 * non-member functions from accessing any of these members, but even if it
	 * didn't, these pieces are considered subject to change without notice,
	 * and making manual modifications might break the class invariant,
	 * causing all sorts of problems.
	 * */

	/* How to represent a vector with only primitive types and arrays?
	 * what data do we need to store? */
	size_t size; // store how much stuff is in the vector.
	size_t capacity; // store the actual size of the underlying array
	int* data; // we'll allocate the actual array later...
};
