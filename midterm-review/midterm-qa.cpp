
string input;

while(getline(cin,input)) {
    if (input == "") {
        break;
    }
}




// Real values
int x = 12;
int y = 23;

// Pointer values
int* p = &x;

cout << *p << endl;
cout << x << endl;

*p = 5;
cout << *p << endl;
cout << x << endl;

p = &y;

cout << *p << endl;
cout << x << endl;

(*p)++;

cout << y << endl;




int f(int a, int &b, int* c) {
    a++;
    b++;
    (*c)++;
}

int main() {
    int x = 1, y = 2, z = 3;
    f(x,y, &z);
    cout << x << endl; // 1
    cout << y << endl; // 3
    cout << z << endl; // 4
}






